package org.yevheniy.test.ticket_payment.controller;


import org.junit.jupiter.api.Test;
import org.yevheniy.test.ticket_payment.model.PaymentStatus;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentGatewayControllerTest {

    private PaymentGatewayController paymentGatewayController = new PaymentGatewayController();

    @Test
    void getRandomStatus() {
        //GIVEN
        Random random = new Random();
        PaymentStatus[] statuses = PaymentStatus.values();
        int nextInt = random.nextInt(statuses.length);
        //WHEN
        String expected = statuses[nextInt].toString();
        String actual = paymentGatewayController.getRandomStatus();
        //THEN
        assertEquals(expected, actual);

    }
}
