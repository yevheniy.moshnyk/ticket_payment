package org.yevheniy.test.ticket_payment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.yevheniy.test.ticket_payment.dto.PaymentDto;
import org.yevheniy.test.ticket_payment.service.PaymentService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping(value = "/payment")
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @GetMapping("/{id}")
    public PaymentDto getPaymentById(@PathVariable(name = "id") UUID id) {
        return paymentService.getById(id);
    }

    @PostMapping
    public UUID createPayment(@Valid @RequestBody PaymentDto paymentDto) {
        return paymentService.create(paymentDto);
    }

    @ExceptionHandler(RuntimeException.class)
    public String entityNotFoundHandler(RuntimeException e) {
        return e.getMessage();
    }
}
