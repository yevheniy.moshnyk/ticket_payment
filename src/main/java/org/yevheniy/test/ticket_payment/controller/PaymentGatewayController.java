package org.yevheniy.test.ticket_payment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yevheniy.test.ticket_payment.service.PaymentGatewayService;

@RestController
@RequestMapping(value = "/paymentGateway")
public class PaymentGatewayController {

    @Autowired
    private PaymentGatewayService paymentGatewayService;

    @GetMapping(value = "/randomStatus")
    public String getRandomStatus() {
        return paymentGatewayService.getRandomStatus().toString();
    }
}
