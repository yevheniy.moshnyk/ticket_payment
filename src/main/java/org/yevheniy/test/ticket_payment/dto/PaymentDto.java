package org.yevheniy.test.ticket_payment.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentDto implements Serializable {
    private int routeNumber;
    private LocalDateTime departureTime;
}
