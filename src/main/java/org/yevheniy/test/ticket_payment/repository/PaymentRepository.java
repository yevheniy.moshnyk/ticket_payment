package org.yevheniy.test.ticket_payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.yevheniy.test.ticket_payment.model.Payment;
import org.yevheniy.test.ticket_payment.model.PaymentStatus;

import java.util.List;
import java.util.UUID;

public interface PaymentRepository extends JpaRepository<Payment, UUID> {

    @Query("select p from Payment p where p.paymentStatus = :status")
    List<Payment> findProcessingPayments(@Param("status") PaymentStatus paymentStatus );
}
