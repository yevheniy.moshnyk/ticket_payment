package org.yevheniy.test.ticket_payment.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.yevheniy.test.ticket_payment.model.Payment;
import org.yevheniy.test.ticket_payment.model.PaymentStatus;
import org.yevheniy.test.ticket_payment.repository.PaymentRepository;

import java.util.List;

@Component
public class PaymentScheduler {

    private static final Logger LOG = LoggerFactory.getLogger(PaymentScheduler.class);

    private static final int DELAY = 60000;
    final String uriRandomStatus = "http://localhost:8080//paymentGateway/randomStatus";

    @Autowired
    private PaymentRepository paymentRepository;

    private RestTemplate restTemplate = new RestTemplate();

    @Scheduled(fixedDelay = DELAY)
    public void run() {
        LOG.info("Start Payment Schedule");
        List<Payment> payments = paymentRepository.findProcessingPayments(PaymentStatus.IN_PROGRESS);
        LOG.info("Founded processing payments: " + payments.size());
        payments.forEach(payment -> payment.setPaymentStatus(getRandomStatus()));
        paymentRepository.saveAll(payments);
        LOG.info("Finish Payment Scheduler");
    }

    private PaymentStatus getRandomStatus() {
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(uriRandomStatus, String.class);
        return PaymentStatus.valueOf(responseEntity.getBody());
    }


}
