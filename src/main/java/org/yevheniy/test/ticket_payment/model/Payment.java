package org.yevheniy.test.ticket_payment.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name = "payments")
public class Payment implements Serializable {

    @Id
    @Column(length = 16)
    private UUID payment_id = UUID.randomUUID();

    @Column(name = "route_number")
    private int routeNumber;

    @Column(name = "departure_time")
    private LocalDateTime departureTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 30)
    private PaymentStatus paymentStatus;

}
