package org.yevheniy.test.ticket_payment.model;

public enum PaymentStatus {
    IN_PROGRESS,
    ERROR,
    COMPLETE
}
