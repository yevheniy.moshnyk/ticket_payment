package org.yevheniy.test.ticket_payment.service;

import org.yevheniy.test.ticket_payment.model.PaymentStatus;

public interface PaymentGatewayService {

    PaymentStatus getRandomStatus();
}
