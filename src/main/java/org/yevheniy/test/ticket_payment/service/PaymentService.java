package org.yevheniy.test.ticket_payment.service;

import org.yevheniy.test.ticket_payment.dto.PaymentDto;

import java.util.UUID;

public interface PaymentService {

    PaymentDto getById(UUID id);

    UUID create(PaymentDto paymentDto);
}
