package org.yevheniy.test.ticket_payment.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yevheniy.test.ticket_payment.converter.PaymentConverter;
import org.yevheniy.test.ticket_payment.dto.PaymentDto;
import org.yevheniy.test.ticket_payment.exception.PaymentException;
import org.yevheniy.test.ticket_payment.model.Payment;
import org.yevheniy.test.ticket_payment.model.PaymentStatus;
import org.yevheniy.test.ticket_payment.repository.PaymentRepository;
import org.yevheniy.test.ticket_payment.service.PaymentService;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class PaymentServiceImpl implements PaymentService {

    private static final Logger LOG = LoggerFactory.getLogger(PaymentServiceImpl.class);

    private final PaymentRepository paymentRepository;
    private PaymentConverter<Payment, PaymentDto > paymentConverter;

    @Autowired
    public PaymentServiceImpl(PaymentRepository paymentRepository, PaymentConverter<Payment, PaymentDto> paymentConverter) {
        this.paymentRepository = paymentRepository;
        this.paymentConverter = paymentConverter;
    }

    @Override
    public PaymentDto getById(UUID id) {
        PaymentDto paymentDto = paymentConverter.toDto(paymentRepository.findById(id).orElseThrow(
                () -> new PaymentException("Payment not found!")));
        LOG.info("Payment by id: " + id + " : " + paymentDto);
        return paymentDto;
    }

    @Override
    public UUID create(PaymentDto paymentDto) {
        if (paymentDto.getDepartureTime().isBefore(LocalDateTime.now())) {
            throw new PaymentException("Wrong date!");
        }
        Payment payment = paymentConverter.toEntity(paymentDto);
        payment.setPaymentStatus(PaymentStatus.IN_PROGRESS);
        paymentRepository.save(payment);
        LOG.info("Created payment: " + payment);
        return payment.getPayment_id();
    }
}
