package org.yevheniy.test.ticket_payment.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.yevheniy.test.ticket_payment.model.PaymentStatus;
import org.yevheniy.test.ticket_payment.service.PaymentGatewayService;

import java.util.Random;

@Service
public class PaymentGatewayServiceImpl implements PaymentGatewayService {

    private static final Logger LOG = LoggerFactory.getLogger(PaymentGatewayServiceImpl.class);

    @Override
    public PaymentStatus getRandomStatus() {
        PaymentStatus[] statues = PaymentStatus.values();
        PaymentStatus paymentStatus = statues[new Random().nextInt(statues.length)];
        LOG.info("Payment gateway got status: " + paymentStatus);
        return paymentStatus;
    }
}
