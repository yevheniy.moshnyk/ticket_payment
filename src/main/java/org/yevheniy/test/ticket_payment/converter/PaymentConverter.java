package org.yevheniy.test.ticket_payment.converter;

public interface PaymentConverter<E, D> {

    D toDto(E e);

    E toEntity(D d);
}
