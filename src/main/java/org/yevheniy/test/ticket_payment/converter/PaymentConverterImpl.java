package org.yevheniy.test.ticket_payment.converter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.yevheniy.test.ticket_payment.dto.PaymentDto;
import org.yevheniy.test.ticket_payment.model.Payment;

@Component
public class PaymentConverterImpl implements PaymentConverter<Payment, PaymentDto> {

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public PaymentDto toDto(Payment payment) {
        return modelMapper.map(payment, PaymentDto.class);
    }

    @Override
    public Payment toEntity(PaymentDto paymentDto) {
        return modelMapper.map(paymentDto, Payment.class);
    }
}
